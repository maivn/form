package form

import (
	"encoding/json"
	"net/url"
	"strings"
)

type Decoder struct {
	prepareValuesFunc func(values url.Values) map[string]interface{}
}

func NewDecoder() *Decoder {
	return &Decoder{}
}

func (d *Decoder) SetPrepareValuesFunc(prepareValuesFunc func(values url.Values) map[string]interface{}) {
	d.prepareValuesFunc = prepareValuesFunc
}

func (d *Decoder) Decode(values url.Values, v interface{}) error {
	if d.prepareValuesFunc == nil {
		d.prepareValuesFunc = defaultPrepareValuesFunc
	}

	body, err := json.Marshal(d.prepareValuesFunc(values))
	if err != nil {
		return err
	}
	return json.Unmarshal(body, v)
}

func defaultPrepareValuesFunc(values url.Values) map[string]interface{} {
	m := make(map[string]interface{})
	for rawKey, value := range values {
		keys := strings.Split(strings.ReplaceAll(rawKey, "]", ""), "[")
		tek := m
		for i, key := range keys {
			switch len(keys) {
			case i + 1:
				tek[key] = value[0]
			default:
				if mm, has := tek[key]; has {
					if n, ok := mm.(map[string]interface{}); ok {
						tek[key] = n
						tek = n
					}
				} else {
					n := make(map[string]interface{})
					tek[key] = n
					tek = n
				}
			}
		}
	}
	return m
}
